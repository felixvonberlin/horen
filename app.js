window.addEventListener("load", function () {
    document.getElementById("submit").addEventListener("click",function(e){
        e.preventDefault();
    
        let date_input = document.getElementById("idob").value;
        let time_input = document.getElementById("itob").value;
        let dob = new Date(Date.parse(date_input + " " + time_input));
        save(dob);
        renderAgeLoop();
    });

    function save(dob)
    {
        localStorage.dob = dob.getTime();
    };

    function load()
    {
        let query = document.location.search.substring(1);
        let params = {};
        let parts = query.split(/&/);
        for (let i in parts) {
            let t = parts[i].split(/=/);
            params[decodeURIComponent(t[0])] = decodeURIComponent(t[1]);
        }
        if ("birthdate" in params) {
            let dob = new Date(Date.parse(params["birthdate"]));
            console.log(dob);
            save(dob);
            return dob;
        }
        let dob = localStorage.getItem("dob");
        if (dob != undefined)
        {
            let rdob = new Date(parseInt(dob));
            console.log(rdob);
            return rdob;
        }

        
        return -1;
    };

    function renderAgeLoop()
    {
        let dob = load();
        document.getElementById("choose").style.display =  "none";
        document.getElementById("timer" ).style.display =  "block";

        setInterval(function(){
            let age = getAge(dob);
            document.getElementById("age").innerHTML = (age.year + "<sup>." + age.fraction + "</sup>");
        }, 100);
    };

    function renderChoose()
    {
        document.getElementById("choose").style.display = "block";
    };

    function getCurrentYearDuration() {
        let currentDate = new Date();
        let clonedDate  = new Date(currentDate.getTime());
        clonedDate.setFullYear(currentDate.getFullYear() - 1);
        return currentDate - clonedDate; 
    }
    function getAge(dob){
        const YEAR      = getCurrentYearDuration();
        const now       = new Date();
        let   dob_mod   = new Date(dob.getTime());
        let   age       = "";
        dob_mod.setFullYear(now.getFullYear());

        const years  = Number(new Date().getFullYear()) - Number(dob.getFullYear())
        const diff   = (now - dob_mod) / YEAR;

        if (diff >= 1) {
            age = years + 1;
        } else {
            age = years;
        }
        age = age + diff;

        let majorMinor = Number(age).toFixed(9).toString().split('.');

        return {
            "year": majorMinor[0],
            "fraction": majorMinor[1]
        };
    };

    function main() {
        if (load() != -1)
        {
            renderAgeLoop();
        } else {
            renderChoose();
        }
    };
    main();
});


function share() {
      navigator.share({
        title: document.title,
        text: "Horen - your age",
        url: window.location.href
      })
      .then(() => console.log('Successful share'))
      .catch(error => console.log('Error sharing:', error));
}
